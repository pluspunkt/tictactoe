#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define MATCH(X1, Y1, X2, Y2, X3, Y3) board[(X1)][(Y1)] == board[(X2)][(Y2)] && board[(X1)][(Y1)] == board[(X3)][(Y3)]
#define RND(MAX) rand() % ((MAX) + 1)
#define xyToN(X, Y) (Y)*3 + (X) + 1

typedef enum tile{
    EMPTY, X, O
}Tile;

typedef Tile Board[3][3];

void clearBoard(Board board){
    for(int y = 0; y < 3; y++){
        for(int x = 0; x < 3; x++){
            board[x][y] = EMPTY;
        }
    }
}

void nToXY(int n, int* x, int* y){
    *x = (n-1) % 3 + 1;
    *y = 1;
    while(n > 3){
        *y += 1;
        n -= 3;
    }
}

void printBoard(Board board, bool xTurn){
    system("clear");
    printf("%s's turn:\n", (xTurn) ? ("\x1b[31mX\x1b[0m") : ("\x1b[34mO\x1b[0m"));
    printf("┌───┬───┬───┐\n");
    for(int y = 0; y < 3; y++){
        for(int x = 0; x < 3; x++){
            printf("│");

            switch(board[x][y]){
                case EMPTY: printf(" %d ", xyToN(x, y)); break;
                case X: printf(" \x1b[31mX\x1b[0m "); break;
                case O: printf(" \x1b[34mO\x1b[0m "); break;
            }
        }
        printf("│\n");
        if(y != 2) printf("├───┼───┼───┤\n");
    }
    printf("└───┴───┴───┘\n");
}

void playerMove(Board board, Tile player){
    int x, y;

    do{
        printf("Enter Value\n");
        
        nToXY(getchar() - '0', &x, &y);
        getchar();
    }while(x < 1 || y < 1 || x > 3 || y > 3 || board[x - 1][y - 1]);

    board[x - 1][y - 1] = player;
}

void randomMove(Board board, Tile player){
    int x, y;

    do{
        x = RND(2);
        y = RND(2);
    }while(board[x][y]);

    board[x][y] = player;
}

Tile winner(Board board){
    for(int i = 0; i < 3; i++){
        if(board[0][i])
            if(MATCH(0, i, 1, i, 2, i)) 
                return board[0][i];

        if(board[i][0])
            if(MATCH(i, 0, i, 1, i, 2)) 
                return board[i][0]; 
    }

    if(board[0][0])
        if(MATCH(0, 0, 1, 1, 2, 2)) 
            return board[0][0];
    
    if(board[2][0])
        if(MATCH(2, 0, 1, 1, 0, 2)) 
            return board[2][0];
    
    return EMPTY;
}

int main(){
    srand(time(NULL));
    
    Board board;
    clearBoard(board);
    
    int moves = 0;
    bool xTurn = true;

    void (*xMover)(Board, Tile) = &playerMove;
    void (*yMover)(Board, Tile) = &randomMove;

    while(moves < 9){
        printBoard(board, xTurn);
        if(xTurn) (*xMover)(board, (xTurn) ? (X) : (O));
        else (*yMover)(board, (xTurn) ? (X) : (O));

        if(winner(board)){
            printBoard(board, xTurn);
            printf("%s won!\n", (winner(board) == X) ? ("\x1b[31mX\x1b[0m") : ("\x1b[34mO\x1b[0m"));
            break;
        }

        xTurn = !xTurn;
        moves++;
    }

    return 0;
}
